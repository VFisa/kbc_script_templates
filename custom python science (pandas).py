__author__ = 'Martin Fiser'
__credits__ = 'Martin Fiser, 2017, Twitter: @VFisa'

# Import Libraries
import os
import pandas as pd

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
#print script_path

LOCAL_DEBUG = 0 # 0 is live data, 1 is local file

if LOCAL_DEBUG:
    filein = "data.csv"
    fileout = "output_data.csv"
else:
    ## initialize application
    from keboola import docker
    cfg = docker.Config('/data/')
    filein = "/data/in/tables/data.csv"
    fileout = "/data/out/tables/data.csv"

    ## access the supplied values, put this into parameter window:
    """
    {
        "parameter_one":"value"
    }
    """
    #params = cfg.get_parameters()
    #parameter_one = cfg.get_parameters()["parameter_one"] # example



def output_file(output_model):
    """
    Save output data as CSV (pandas)
    """

    output_model.to_csv(fileout, index=False)
    print("INFO: file produced.")


def input_file(file_in, debug=False):
    """
    Read input data as CSV DataFrame (pandas)
    """

    if debug:
        frame = pd.read_csv(filein, nrows=2000, encoding='utf-8')
    else:
        frame = pd.read_csv(file_in, encoding='utf-8')
    print("INFO: input file read.")

    return frame


model = input_file(filein)

## TODO
output_model = model

output_file(output_model)

print("INFO: script completed.")