__author__ = 'Martin Fiser'
__credits__ = 'Martin Fiser, 2017, Twitter: @VFisa'

# Import Libraries
import os
import csv

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
#print script_path

LOCAL_DEBUG = 0 # 0 is live data, 1 is local file

if LOCAL_DEBUG:
    filein = "data.csv"
    fileout = "output_data.csv"
else:
    ## initialize application
    from keboola import docker
    cfg = docker.Config('/data/')
    filein = "/data/in/tables/data.csv"
    fileout = "/data/out/tables/data.csv"

    ## access the supplied values
    #params = cfg.get_parameters()
    #parameter_one = cfg.get_parameters()["parameter_one"] # example



def output_file(output_model):
    """
    Save output data as CSV (without pandas)
    """

    with open(fileout, 'wb') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in output_model:
            spamwriter.writerow(row)
    print("INFO: file produced.")


def input_file(file_in):
    """
    Read input data as CSV DataFrame (without pandas)
    """
    with open(filein, 'rb') as csvfile:
        file_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        input_data = list(file_reader)
    print("INFO: input file read.")

    return input_data


model = input_file(filein)

## TODO
output_model = model

output_file(output_model)

print("INFO: script completed.")